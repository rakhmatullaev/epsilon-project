# Generated by Django 3.1 on 2020-08-18 04:23

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('slug', models.SlugField(unique=True, verbose_name='link')),
                ('image', models.ImageField(blank=True, null=True, upload_to='', verbose_name='image')),
                ('content', models.TextField(verbose_name='text')),
                ('date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date')),
                ('publish', models.BooleanField(default=False, verbose_name='Publicated')),
            ],
        ),
    ]
