from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User

class RegisterForm(UserCreationForm):
	username = forms.CharField(label='nickname', required=True, widget=forms.TextInput(attrs={'class': 'form-control mb-20'}))
	password1 = forms.CharField(label='password1', required=True, widget=forms.PasswordInput(attrs={'class': 'form-control mb-20'}))
	password2 = forms.CharField(label='password(again)', required=True, widget=forms.PasswordInput(attrs={'class': 'form-control mb-20'}))
	email = forms.EmailField(label='email', required=True, widget=forms.TextInput(attrs={'class': 'form-control mb-20'}))
	first_name = forms.CharField(label='name', required=True, widget=forms.TextInput(attrs={'class': 'form-control mb-20'}))
	last_name = forms.CharField(label='surname', required=True, widget=forms.TextInput(attrs={'class': 'form-control mb-20'}))

	class Meta():
		model = User
		fields = ('username','email','password1','password2','first_name','last_name',)

	def save(self, commit=True):
		user = super().save(commit=False)
		user.email = self.cleaned_data['email']
		user.first_name = self.cleaned_data['first_name']
		user.last_name = self.cleaned_data['last_name']
		if commit:
			user.save()
			return user












