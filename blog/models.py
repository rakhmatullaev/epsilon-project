from django.db import models
from django.utils import timezone
from django.shortcuts import reverse
from django.contrib.auth.models import User

class Category(models.Model):
	title = models.CharField('title', max_length = 255)
	slug = models.SlugField('link', unique = True)
	image = models.ImageField('image', blank = True, null = True)

	# Class Meta for russian language)
	class Meta:
		verbose_name = 'Category'
		verbose_name_plural = 'Categories'

	def get_absolute_url(self):
		return reverse('category_detail_url', kwargs={'slug': self.slug})

	def __str__(self):
		return self.title

class Post(models.Model):
	title = models.CharField('title', max_length = 255)
	slug = models.SlugField('link', unique = True)
	image = models.ImageField('image', blank = True, null = True)
	category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True, verbose_name = 'Category')
	content = models.TextField('text')
	date = models.DateTimeField('Date', default = timezone.now)
	publish = models.BooleanField('Publicated', default = True)
	views = models.IntegerField('Views', default=0)

	# Class Meta for russian language)
	class Meta:
		verbose_name = 'news'
		verbose_name_plural = 'news'

	def get_absolute_url(self):
		return reverse('post_detail_url', kwargs={'slug': self.slug})

	def __str__(self):
		return self.title

class Comment(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Author')
	post = models.ForeignKey(Post, on_delete=models.CASCADE, verbose_name='News')
	text = models.TextField('Comment')
	date = models.DateTimeField('Date', default=timezone.now)

	class Meta:
		verbose_name = 'comment'
		verbose_name_plural = 'comments'

	def __str__(self):
		return self.user.username






